#!/usr/bin/env bash

# fetch N subtrees from a remote repo
if [[ ! "$#" -eq 3 ]]; then
    echo -e "invalid invocation: $@\n\n"
    echo -e "USAGE:"
    echo -e "\t$0 <output dir> <git repo> <tag>"
    exit 1
fi

OUTPUT_DIR=$1 && shift
URL=$1 && shift
TAG=$1 && shift

if [ ! -e $OUTPUT_DIR ]; then
    git clone $URL $OUTPUT_DIR
fi

`dirname $0`/fetch_remote_tree.sh -o $OUTPUT_DIR -u $URL -t $TAG
