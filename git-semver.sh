#! /bin/bash

if [ "$1" != "" ]; then
    TARGET_DIR=$1
else
    TARGET_DIR=`pwd`
fi

pushd $TARGET_DIR > /dev/null

DESCRIBE=`git describe --tags | tr -d v`
if [ "$DESCRIBE" = "" ]; then exit 1; fi

# split into sections
VERSION=`echo $DESCRIBE | awk '{split($0,a,"-"); print a[1]}'`
BUILD=`echo $DESCRIBE | awk '{split($0,a,"-"); print a[2]}'`
COMMIT=`echo $DESCRIBE | awk '{split($0,a,"-"); print a[3]}' | tr -d g`

MAJOR=`echo $VERSION | awk '{split($0,a,"."); print a[1]}'`
MINOR=`echo $VERSION | awk '{split($0,a,"."); print a[2]}'`
PATCH=`echo $VERSION | awk '{split($0,a,"."); print a[3]}'`

if [ "${BUILD}" = "" ]; then
   # NO OP
    PRERELEASE=""
else
    # bump patch version
    PATCH=$(expr $PATCH + 1)
    PRERELEASE=-dev-$BUILD-$COMMIT
fi

VERSION=$MAJOR.$MINOR.$PATCH
echo ${VERSION}${PRERELEASE}

popd > /dev/null
