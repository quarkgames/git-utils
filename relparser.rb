#!/usr/bin/env ruby


if ARGV.length < 3
    throw "usage: [repo name] previous..current [repo directory]"
end

repo_name = ARGV[0]
log_range = ARGV[1]
directory = ARGV[2]
directory = File.expand_path(directory)
Dir.chdir(directory)

CODE_EXTENSION_TYPES = [".cs", ".rb", ".erl", ".ex", ".exs"]
BUG_PREFIX = "NIM"
PULL_REQUEST_URL_BASE = "https://bitbucket.org/quarkgames/#{repo_name}/pull-request/"

phab_revisions = []
no_code_changes = []
remaining = []
merge_hashes = {}

hashes = `git log #{log_range} --pretty=format:"%h"`
hashes.each_line do |hash|
    hash = hash.chomp
    parents = `git rev-list #{hash}...#{hash}~
                            #--abbrev-commit`.gsub(/\s+/m, ' ').strip.split(" ")
    body = `git show #{hash} -s --pretty="format:%B"`.chomp
    author = `git show #{hash} -s --pretty="format:%aE"`.chomp
    subject = `git show #{hash} -s --pretty="format:%s"`.chomp
    files_changed = `git show --pretty="format:" --name-only #{hash}`

    # diff revision
    revision_match = body.match('pull request #([0-9]+)')

    commit_data = {
        "hash" => hash,
        "body" => body,
        "subject" => subject,
        "author" => author.match('(.*?)@').values_at(1)[0]
    }

    # merge commit
    if parents.length > 1

        commit_data["issues"] = []
        if revision_match
            commit_data["revision"] = revision_match.values_at(1)[0]
        else 
            commit_data["revision"] = ""
        end

        bug_data = body.match("#{BUG_PREFIX}-[0-9]+")
        if bug_data 
            commit_data["issues"] = bug_data.to_a.uniq
        end

        phab_revisions << commit_data
        parents.each {|parent| merge_hashes[parent] = ""}
    elsif CODE_EXTENSION_TYPES.select {|extension| files_changed.include? extension }.empty?
        # no code changes
        no_code_changes << commit_data
    else 
        unless merge_hashes[hash]
            remaining << commit_data
        end
    end
end

puts "### Change Log\n\n"
puts "#### Revisions\n\n"

phab_revisions.each do |commit|

    issues = commit["issues"]
    issues = issues.collect {|bug| "[#{bug}](https://quarkgames.atlassian.net/browse/#{bug})" }
    issues_formatted = issues.empty? ? "" : "(References: #{issues.join(',')})"

    commit_label = "Pull Request ##{commit["revision"]}"

    commit_formatted = ""
    unless commit["revision"].empty?
        commit_formatted = "[#{commit_label}](#{PULL_REQUEST_URL_BASE}#{commit["revision"]})"
    end

    puts "* #{commit['subject']} #{issues_formatted}
            ##{commit_formatted} (#{commit['hash']}
            ###{commit["author"]})"
end

puts "\n\n"
puts "#### Non-code Commits\n\n"

no_code_changes.each do |commit|
    puts "* #{commit['subject']} (#{commit['hash']}
            ##{commit["author"]})"
end

puts "\n\n"
puts "#### Uncovered Code commits \n\n"

remaining.each do |commit|
    puts "* #{commit['subject']} (#{commit['hash']}
            ##{commit["author"]})"
end
