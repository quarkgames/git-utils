#!/usr/bin/env bash

# fetch N subtrees from a remote repo
#if [[ "$#" -lt 4 ]]; then
 #   usage()
#fi

usage() {
    echo -e "USAGE:"
    echo -e "\t$0 -o <output dir> -u <git url> -r <revision> -t <tag prefix> [<subtree>]"
    exit 1
}

while getopts "ho:u:t:r:" opt; do
    case $opt in
        h)
            usage
            ;;
        o)
            OUTPUT_DIR=$OPTARG
            ;;
        u)
            GIT_URL=$OPTARG
            ;;
        r)
            REVISION=$OPTARG
            ;;
        t)
            TAG=$OPTARG
            ;;
    esac
done

[[ ! $TAG && ! $REVISION ]] && usage
[[ ! $GIT_URL ]] && usage
[[ ! $OUTPUT_DIR ]] && usage

shift $((OPTIND-1))
SUBTREES=$@

REMOTE="origin"

mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR
if [[ -e ".git" ]]; then
    CMD=$(git remote show $REMOTE | grep $GIT_URL)
    if [[ $CMD ]]; then
        echo "updating existing repo"
    else
        echo "remote url mismatch!"
        exit 2
    fi
else
    git init
    git remote add -f $REMOTE $GIT_URL
fi


if [[ $SUBTREES ]]; then
    git config core.sparsecheckout true
    echo "" > .git/info/sparse-checkout
    for tree in $SUBTREES
    do
        echo "$tree" >> .git/info/sparse-checkout
    done
fi

git fetch $REMOTE
git fetch --tags

if [[ $TAG != "" ]]; then
    REF=$(git tag -l $TAG* | sort -t. -k 1,1n -k 2,2n -k 3,3n | tail -n 1)
    if [ "$REF" == "" ]; then
        echo "There is no matching tag for $TAG"
        exit 1
    fi
elif [[ $REVISION != "" ]]; then
    REF=$REVISION
fi

git checkout -f $REF
git reset --hard HEAD
git clean -f


EXPECTED_REF=$(git rev-parse HEAD)
ACTUAL_REF=$(git rev-parse $REF)
if [[ $EXPECTED_REF != $ACTUAL_REF ]]; then
    echo "Invalid ref $ACTUAL_REF, expected $EXPECTED_REF"
    exit 1
fi
